#!/bin/bash

if [ $# -eq 0 ]; then
  # No argument provided. Exec jupyter notebook
  exec jupyter notebook --port 8888 --ip 0.0.0.0
fi

# There are some argument. Exec the given argument instead
exec $@
