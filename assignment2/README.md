# Docker image for assignment 2

## Usage

First `cd` to a directory storing your notebooks, then run the following snippets:

```bash
docker run -v `pwd`:/app -p 8888:8888 registry.gitlab.com/holi0317/comp-4471-images/assignment2
```

Open http://localhost:8888 to access the jupyter notebook environment.
